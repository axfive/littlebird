extends CharacterBody3D

@export var speed: float = 5.0
@export var jump_speed: float = 5.0
@export var camera_: NodePath
@onready var camera: Camera3D = get_node(camera_)
@onready var animation_player: AnimationPlayer = $Pivot/female/AnimationPlayer

var gravity = ProjectSettings.get_setting('physics/3d/default_gravity') * ProjectSettings.get_setting('physics/3d/default_gravity_vector')

enum AnimationState {
	IDLE,
	WALK,
}

var animation_state = AnimationState.IDLE

# Called when the node enters the scene tree for the first time.
func _ready():
	up_direction = (-gravity).normalized()
	animation_player.get_animation('Walk').loop_mode = Animation.LOOP_LINEAR
	animation_player.stop(true)

func _physics_process(delta: float):
	var movement_direction = Vector3(Input.get_axis(&"move_left", &"move_right"), 0.0, Input.get_axis(&"move_forward", &"move_backward"))
	var camera_basis = camera.global_transform.basis
	camera_basis = camera_basis.rotated(camera_basis.x, -camera_basis.get_euler().x)
	var movement = camera_basis * movement_direction

	var new_animation_state = AnimationState.IDLE
	movement.y = 0

	velocity += gravity * delta

	if is_on_floor():
		var movement_length = movement.length()
		if movement_length > 1.0:
			movement /= movement_length

		#look_at(transform.origin + movement * speed, up)

		velocity.x = movement.x * speed
		velocity.z = movement.z * speed
		if movement_length > 0.02:
			rotation.y = atan2(velocity.x, velocity.z)
			animation_player.playback_speed = movement_length
			new_animation_state = AnimationState.WALK
			
		if Input.is_action_just_pressed('jump'):
			animation_player.stop(true)
			velocity.y = jump_speed
			new_animation_state = AnimationState.IDLE
	
	if animation_state != new_animation_state:
		animation_state = new_animation_state
		if animation_state == AnimationState.IDLE:
			animation_player.stop(true)
		elif animation_state == AnimationState.WALK:
			animation_player.play("Walk")

	move_and_slide()
